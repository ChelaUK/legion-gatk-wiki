= ICH analysis Pipeline (on Legion Cluster) =
\\
Table of Contents:
\\

== Introduction ==

* [[Welcome.wiki|Introduction]]

\\
== Setting up the Pipeline ==

* [[Basics.wiki|Basic setup]]
* [[Files.wiki|Preparing the files]]

\\
== Running the Pipeline ==

* [[Common.wiki|Common Principles]]
* [[Alignment.wiki|Alignment]]
* [[Coverage.wiki|Coverage statistics]]
* [[Gatk.wiki|GATK pipeline]]
* [[Dindel.wiki|Dindel Analysis]]
* [[Annotation.wiki|Annotation of the Variants]]
* [[CNV.wiki|CNV Analysis and annotation]]

\\